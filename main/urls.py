from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name="index"),
    url(r'^api/contact/$', views.contactList, name="contact_list"),
    url(r'^api/contact/(?P<id>[0-9]+)/$', views.contactDetail, name="contact_detail"),
    url(r'^api/message/$', views.message, name="message"),
]
