/**
 * Created by nidhi on 22/1/17.
 */

var mainApp = angular.module('mainApp', ['ui.router']);

mainApp.config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/contacts');

    $stateProvider
        .state('contacts', {
            url: '/contacts',
            templateUrl: 'static/main/template/list.html',
            controller: 'contactListController'
        })

        .state('contact', {
            url: '/contact/:id',
            templateUrl: 'static/main/template/detail.html',
            controller: 'contactDetailController'
        })

        .state('compose', {
            url: '/compose/:id',
            templateUrl: 'static/main/template/compose.html',
            controller: 'messageComposeController'
        })

        .state('sms', {
            url: '/sms',
            templateUrl: 'static/main/template/sms.html',
            controller: 'messageListController'
        })
});