/**
 * Created by nidhi on 23/1/17.
 */

(function(){
    angular.module('mainApp')

        .controller('contactDetailController', ['$http', '$scope', '$stateParams', function($http, $scope, $stateParams){
            $http({
                url: "/api/contact/" + $stateParams.id + "/",
                method: "GET",
                dataType: "JSON",
            }).success(function(response){
                console.log("Success");
                $scope.response = response;
            }).error(function(){
               console.log("Error");
            });
        }])
})();