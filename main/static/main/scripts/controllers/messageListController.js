/**
 * Created by nidhi on 23/1/17.
 */
(function(){
    angular.module('mainApp')

        .controller('messageListController', ['$http', '$scope', function($http, $scope){
            $http({
                url: "/api/message/",
                method: "GET",
                dataType: "JSON",
            }).success(function(response){
                console.log("Success");
                console.log(response);
                $scope.response = response;
            }).error(function(){
               console.log("Error");
            });
        }])
})();
