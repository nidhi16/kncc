/**
 * Created by nidhi on 23/1/17.
 */
(function(){
    angular.module('mainApp')

        .controller('messageComposeController', ['$http', '$scope', '$stateParams', function($http, $scope, $stateParams){
            $http({
                url: "/api/contact/" + $stateParams.id + "/",
                method: "GET",
                dataType: "JSON",
            }).success(function(response){
                $scope.response = response;
                $scope.text = "Hi, Your OTP is: " + ("" + Math.random()).substring(2,8);

                $("#send_message").click(function() {
                    console.log('click for send');
                    console.log($stateParams.id);
                    console.log($scope.text);
                    $.post("/api/message/", {'id': $stateParams.id, 'text': $scope.text})
                        .done(function(data) {
                            console.log('response done');
                            console.log(data);
                            if (data.status == 'failure') {
                                var dialog = new BootstrapDialog({
                                title: function(dialogRef) {return $('<div>Success</div>');},
                                            message: function(dialogRef){
                                                return $('<div>Error sending SMS</div>');
                                            }
                                        });
                                dialog.realize();
                                dialog.getModalHeader().css('background-color', '#FF9494');
                                dialog.open();
                            } else {
                                var dialog = new BootstrapDialog({
                                title: function(dialogRef) {return $('<div>Success</div>');},
                                            message: function(dialogRef){
                                                return $('<div>SMS sent</div>');
                                            }
                                        });
                                dialog.realize();
                                dialog.open();
                            }

                        })
                        .fail(function(data) {
                            console.log('response fail');
                            console.log(data);
                        });
                });
            }).error(function(){
               console.log("Error");
            });
        }])
})();