/**
 * Created by nidhi on 22/1/17.
 */

(function(){
    angular.module('mainApp')

        .controller('contactListController', ['$http', '$scope', function($http, $scope){
            $http({
                url: "/api/contact/",
                method: "GET",
                dataType: "JSON",
            }).success(function(response){
                console.log("Success");
                $scope.response = response;
            }).error(function(){
               console.log("Error");
            });
        }])
})();