from django.shortcuts import render
from .models import Contact, Message
from django.http import HttpResponse, Http404
import json
import requests
from django.views.decorators.csrf import csrf_exempt


def index(request):
    return render(request, 'main/index.html')


def contactList(request):
    contact_list = Contact.objects.all()
    contact_list = list(contact_list.values())
    return HttpResponse(json.dumps(contact_list), content_type='application/json')


def contactDetail(request, id):
    try:
        contact = Contact.objects.get(pk=id)
        data = {
            'id': contact.pk,
            'name': contact.name,
            'phone': contact.phone
        }
        return HttpResponse(json.dumps(data), content_type='application/json')
    except Contact.DoesNotExist:
        return Http404()

@csrf_exempt
def message(request):
    if request.method == "GET":
        message_list = Message.objects.all().order_by('-time')
        data = []
        for message in message_list:
            data.append({
                'id': message.id,
                'name': message.contact.name,
                'phone': message.contact.phone,
                'text': message.text,
                'status': message.status
            })
        return HttpResponse(json.dumps(data), content_type='application/json')
    elif request.method == 'POST':
        try:
            contact_id = request.POST.get('id')
            text = request.POST.get('text')
            contact = Contact.objects.get(pk=contact_id)
            params = {
                'apiKey': 'v42KALX10nQ-oupUDoXtWHplBZqK8JnfLR7mYLGiOL',
                # 'apiKey': 'v42KALX10nQ-oupUDoXtWHplBZqK8JnfLR7mYL',
                'numbers': '91{}'.format(contact.phone),
                'message': '{}'.format(text),
                'sender': 'TXTLCL'
            }
            resp = requests.get('http://api.textlocal.in/send/', params=params)
            msg = Message()
            msg.text = text
            msg.status = json.loads(resp.text)['status']
            msg.contact = contact
            msg.save()
            data = {
                'contact': msg.contact.id,
                'text': msg.text,
                'status': msg.status
            }
            return HttpResponse(json.dumps(data), content_type='application/json')
        except Contact.DoesNotExist:
            return Http404()
    else:
        return HttpResponse(status=405)
