from __future__ import unicode_literals

from django.db import models
from datetime import datetime


class Contact(models.Model):
    phone = models.CharField(max_length=10)
    name = models.CharField(max_length=128)


class Message(models.Model):
    text = models.CharField(max_length=140)
    status = models.CharField(max_length=10)
    contact = models.ForeignKey(Contact)
    time = models.DateTimeField(default=datetime.now)
